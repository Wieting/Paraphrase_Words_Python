import numpy as np
from time import time
from scipy.sparse import csr_matrix
from scipy.sparse import lil_matrix
from scipy.sparse import dok_matrix

def testFancy(mat,idxs):
    for i in idxs:
        x = mat[i,:]

def testTake(mat,idxs):
    for i in idxs:
        x = mat.take(range(i*50,i*50+50))

def testSparse(mat,idxs):
    for i in idxs:
        x = mat.getrow(i)

mat = np.random.rand(50000,50)
idxs = np.random.randint(50000-1, size=1000)

#fancy
t0 = time()
testFancy(mat,idxs)
t1 = time()
print str(t1-t0)

#take
t0 = time()
testTake(mat,idxs)
t1 = time()
print str(t1-t0)

mat = csr_matrix((50000,50))
t0 = time()
testSparse(mat,idxs)
t1 = time()
print str(t1-t0)

mat = lil_matrix((50000,50))
t0 = time()
testFancy(mat,idxs)
t1 = time()
print str(t1-t0)

mat = dok_matrix((50000,50))
t0 = time()
testFancy(mat,idxs)
t1 = time()
print str(t1-t0)


