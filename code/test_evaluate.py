from utils import lookup
from utils import getWordmap
from params import params
from utils import getData
from utils import getPairs
from objective import objective
from gradient import gradient
from check_gradient import computeNumericalGradient
from check_gradient import scoreGradient
from evaluate import evaluate_adagrad
import warnings
import numpy as np

hiddensize = 25
(words, We) = getWordmap('../data/skipwiki25.txt')
We=We/np.sqrt(np.power(We,2).sum(axis=1))
params = params()
examples = getData(params.dataf)
evaluate_adagrad(We,words)