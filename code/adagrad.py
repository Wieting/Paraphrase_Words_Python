import numpy as np
from random import shuffle
from utils import getPairsBatch
from utils import getPairs
from objective import objective
from gradient import gradient
from evaluate import evaluate_adagrad
import sys

def checkIfQuarter(idx,n,batchsize):
    start = idx - batchsize
    for i in range(start,idx):
        if i==round(n/4.) or i==round(n/2.) or i==round(3*n/4.):
            return True
    return False

def saveParams(words,We,outfile,counter):
    #save as text file for now
    outfile = outfile + ".params."+str(counter)+".txt"
    fout = open(outfile,'w')
    for i in words:
        word = i
        vec = We[words[i],:]
        ss = word+"\t"
        for i in vec:
            ss = ss + str(i)+"\t"
        ss=ss.strip()
        fout.write(ss+"\n")
    fout.close()

def evaluateParams(words,We):
    evaluate_adagrad(We,words)

def adagrad(params, words, We):
    old_We = We
    G = np.mat(np.zeros(We.shape))
    delta = 1E-4
    data = params.data
    n = len(data)
    counter = 0

    for i in range(params.epochs):
        shuffle(data)
        pairs = getPairsBatch(data,words,We,params.batchsize,params.type)
        cost = objective(params.hiddensize,data,pairs,words,We,old_We,params.lam,params.margin)
        print "cost at epoch at "+str(i)+" : "+str(cost)
        idx = 0
        while idx < n:
            batch = data[idx: idx + params.batchsize if idx + params.batchsize < len(data) else len(data)]
            idx += params.batchsize
            if(len(batch) <= 2):
                print "batch too small."
                continue #just move on because pairing could go faulty
            pairs = getPairs(batch,words,We,params.type)
            grad = gradient(batch,pairs,words,We,old_We,params.lam,params.margin)
            G = G + np.power(grad,2)
            We = We - params.eta*grad/(np.sqrt(G)+delta)
            if(params.normalize):
                We=We/np.sqrt(np.power(We,2).sum(axis=1))
            if(checkIfQuarter(idx,n,params.batchsize)):
                if(params.save):
                    counter += 1
                    saveParams(words,We,params.outfile,counter)
                if(params.evaluate):
                    evaluateParams(words,We)
                    sys.stdout.flush()
        if(params.save):
            counter += 1
            saveParams(words,We,params.outfile,counter)
        if(params.evaluate):
            evaluateParams(words,We)
            sys.stdout.flush()
