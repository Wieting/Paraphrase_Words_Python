from utils import lookup
from utils import getWordmap
from params import params
from utils import getData
from adagrad import adagrad
import warnings
import numpy as np
from random import shuffle

def limitwords(words,We,d):
    lis = []
    for i in d:
        lis.append(i[0])
        lis.append(i[1])
    lis = list(set(lis))
    newwords = {}
    for (n,i) in enumerate(lis):
        newwords[i]=n
    newWe = []
    for i in lis:
        newWe.append(np.array(We[words[i],:]).tolist()[0])
    return (newwords,np.matrix(newWe))

warnings.filterwarnings("ignore")
hiddensize = 25
(words, We) = getWordmap('../data/skipwiki25.txt')
params = params()
examples = getData(params.dataf)
shuffle(examples)
sample = examples[0:1000]
#(words,We) = limitwords(words,We,sample)

print "Testing adagrad"
params.data = sample
params.epochs=20
params.eta=0.25
params.batchsize=75
print examples
adagrad(params,words,We)