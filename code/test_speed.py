from utils import lookup
from utils import getWordmap
from params import params
from utils import getData
from utils import getPairs
from objective import objective
from gradient import gradient
from check_gradient import computeNumericalGradient
from check_gradient import scoreGradient
import warnings
import numpy as np
from time import time

def testFancy(We):
    arr = np.random.randint(50000, size=1000)
    for i in arr:
        x = We[i,:]

def testTake(We):
    arr = np.random.randint(50000, size=1000)
    for i in arr:
        x = We.take(range(i*25+25))

warnings.filterwarnings("ignore")
hiddensize = 25
(words, We) = getWordmap('../data/skipwiki25.txt')

t0 = time()
testFancy(We)
t1 = time()
print str(t1-t0)

t0 = time()
testTake(We)
t1 = time()
print str(t1-t0)

