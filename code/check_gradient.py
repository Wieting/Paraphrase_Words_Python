from objective import objective
import numpy as np
from numpy.linalg import norm

def computeNumericalGradient(hiddensize,sample,pairs,words,We,We_old,lam,margin):
    f = lambda x : objective(hiddensize,sample,pairs,words,x,We_old,lam,margin)
    epsilon = 1E-4
    theta = We.flatten()
    numgrad = np.zeros((theta.shape))
    for i in range(numgrad.size):
        z = np.zeros((theta.shape))
        z[0][i] = epsilon
        JplusEps = f(theta + z)
        JminusEps = f(theta - z)
        numgrad[0][i] = (JplusEps - JminusEps) / (2*epsilon)
    return numgrad

def scoreGradient(grad,numgrad):
    return norm(grad-numgrad) / (norm(grad+numgrad))