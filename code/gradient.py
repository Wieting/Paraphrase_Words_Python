from scipy.sparse import csr_matrix
from scipy.sparse import dok_matrix
import numpy as np

def gradient(d, pairs, words, We, We_old, lam, margin ):
    (a, b) = We.shape
    grad = dok_matrix((a,b))

    for ii in range(len(d)):
        #gradWe = np.zeros((a,b))
        gradWe = dok_matrix((a,b))
        (t1,t2,label) = d[ii]
        (p1,p2) = pairs[ii]
    
        g1 = We[words[t1],:]
        g2 = We[words[t2],:]
        v1 = We[words[p1],:]
        v2 = We[words[p2],:]

        if(label > 0):
            d1 = margin - np.inner(g1,g2) + np.inner(v1,g1)
            d2 = margin - np.inner(g1,g2) + np.inner(v2,g2)
            if(d1 < 0):
                d1 = 0
            if(d2 < 0):
                d2 = 0

        else:
            #print "HERE"
            d1 = margin + np.inner(g1,g2) + np.inner(v1,g1)
            d2 = margin + np.inner(g1,g2) + np.inner(v2,g2)
            if(d1 < 0):
                d1 = 0
            if(d2 < 0):
                d2 = 0

        if d1 > 0 or d2 > 0:
            allwords = list(set([t1,t2,p1,p2]))
            if d1 > 0:
                for i in allwords:
                    wi = We[words[i],:]
                    (m1,m2,m3) = (False,False,False)

                    if t1 == i:
                        m1 = True
                    if t2 == i:
                        m2 = True
                    if p1 == i:
                        m3 = True

                    #pg = gradWe.getrow(words[i]).todense()
                    pg = gradWe[words[i],:]

                    if label > 0:
                        if(m1):
                            pg = pg - g2
                        if(m2):
                            pg = pg - g1
                    else:
                        if(m1):
                            pg = pg + g2
                        if(m2):
                            pg = pg + g1

                    if(m3):
                        pg = pg + g1
                    if(m1):
                        pg = pg + v1

                    gradWe[words[i],:] = pg
            if d2 > 0:
                for i in allwords:
                    wi = We[words[i],:]
                    (m1,m2,m3) = (False,False,False)

                    if t1 == i:
                        m1 = True
                    if t2 == i:
                        m2 = True
                    if p2 == i:
                        m3 = True

                    #pg = gradWe.getrow(words[i]).todense()
                    pg = gradWe[words[i],:]

                    if label > 0:
                        if(m1):
                            pg = pg - g2
                        if(m2):
                            pg = pg - g1
                    else:
                        if(m1):
                            pg = pg + g2
                        if(m2):
                            pg = pg + g1

                    if(m3):
                        pg = pg + g2
                    if(m2):
                        pg = pg + v2

                    gradWe[words[i],:] = pg
        grad = grad + gradWe
    grad = grad/len(d) - lam*(We_old-We)
    return grad

