from utils import lookup
from utils import getWordmap
from params import params
from utils import getData
from utils import getPairs
from objective import objective
from gradient import gradient
from check_gradient import computeNumericalGradient
from check_gradient import scoreGradient
import warnings
import numpy as np

def limitwords(words,We,d):
    lis = []
    for i in d:
        lis.append(i[0])
        lis.append(i[1])
    lis = list(set(lis))
    newwords = {}
    for (n,i) in enumerate(lis):
        newwords[i]=n
    newWe = []
    for i in lis:
        newWe.append(np.array(We[words[i],:]).tolist()[0])
    return (newwords,np.matrix(newWe))

warnings.filterwarnings("ignore")
hiddensize = 25
(words, We) = getWordmap('../data/skipwiki25.txt')
params = params()
examples = getData(params.dataf)
sample = examples[0:20]
(words,We) = limitwords(words,We,sample)

print "Testing gradients"

pairs = getPairs(sample,words,We,params.type)
    #for i in range(len(sample)):
    #print sample[i][0], pairs[i][0]
    #print np.inner(lookup(We,words,sample[i][0]), lookup(We,words,pairs[i][0]))
    #print sample[i][1], pairs[i][1]
    #print np.inner(lookup(We,words,sample[i][1]), lookup(We,words,pairs[i][1]))

cost = objective(hiddensize,sample,pairs,words,We,We,params.lam,params.margin)
print "cost: "+str(cost)
grad = gradient(sample,pairs,words,We,We,params.lam,params.margin)
#print grad
numgrad = computeNumericalGradient(hiddensize,sample,pairs,words,We,We,params.lam,params.margin)
print "grad score: "+str(scoreGradient(grad.flatten(),numgrad))

#print numgrad
#print grad.flatten() - numgrad.flatten()
#for (n,i) in enumerate(pairs):
#    print i, sample[n]