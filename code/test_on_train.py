import sys
from scipy.spatial.distance import cosine

f = open(sys.argv[1],'r')
lines = f.readlines()

currword = None
currV = []
d={}
for i in lines:
    arr=i.split()
    if "[[" in i:
        if not (currword == None):
            d[currword] = currV
            currV = []
        currword = arr[0]
        ct = 1
        while ct < len(arr):
            if not arr[ct] == "[[":
                currV.append(float(arr[ct].replace("[[","")))
            ct += 1
    else:
        for i in arr:
            if not i == "]]":
                currV.append(float(i.replace("]]","")))
d[currword] = currV

old_d = {}
f = open('data/skipwiki25.txt','r')
lines = f.readlines()
for i in lines:
    i=i.split()
    word = i[0]
    ct = 1
    v = []
    while ct < len(i):
        v.append(float(i[ct]))
        ct += 1
    old_d[word]=v

tr_data = open('data/wn-syn-ant-only.2.txt','r')
tr_lines = tr_data.readlines()

for i in tr_lines:
    i=i.split()
    print i[0], i[1], i[2], -cosine(d[i[0]],d[i[1]])+1, -cosine(old_d[i[0]],old_d[i[1]])+1
