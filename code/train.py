import sys
import warnings
from utils import getWordmap
from params import params
from utils import getData
from adagrad import adagrad

warnings.filterwarnings("ignore")
params = params()
args = sys.argv

params.lam=float(args[1])
params.frac = float(args[2])
params.outfile = args[3]
params.dataf = args[4]
params.batchsize = int(args[5])
params.hiddensize = int(args[6])
params.type = args[7]
params.save = True
params.constraints = False
params.normalize = False

(words, We) = getWordmap('../data/skipwiki25.txt')
params.outfile = "../models/"+params.outfile+"."+str(params.lam)+"."+str(params.batchsize)+".txt"
examples = getData(params.dataf)

params.data = examples[0:int(params.frac*len(examples))]

print "Training on "+str(len(params.data))+" examples using lambda="+str(params.lam)

adagrad(params, words, We)
