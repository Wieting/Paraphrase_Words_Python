sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 50 25 RAND
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 50 25 MIX
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 50 25 MAX
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 50 25 RAND
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 50 25 MIX
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 50 25 MAX
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 50 25 RAND
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 50 25 MIX
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 50 25 MAX
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 75 25 RAND
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 75 25 MIX
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 75 25 MAX
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 75 25 RAND
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 75 25 MIX
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 75 25 MAX
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 75 25 RAND
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 75 25 MIX
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 75 25 MAX
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 100 25 RAND
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 100 25 MIX
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 100 25 MAX
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 100 25 RAND
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 100 25 MIX
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 100 25 MAX
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 100 25 RAND
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 100 25 MIX
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 100 25 MAX
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 200 25 RAND
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 200 25 MIX
sh train.sh 0.0001 1.0 words-ppdb ../data/ppdb-new.2.txt 200 25 MAX
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 200 25 RAND
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 200 25 MIX
sh train.sh 1e-05 1.0 words-ppdb ../data/ppdb-new.2.txt 200 25 MAX
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 200 25 RAND
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 200 25 MIX
sh train.sh 1e-06 1.0 words-ppdb ../data/ppdb-new.2.txt 200 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 50 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 50 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 50 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 50 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 50 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 50 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 50 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 50 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 50 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 75 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 75 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 75 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 75 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 75 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 75 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 75 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 75 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 75 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 100 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 100 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 100 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 100 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 100 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 100 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 100 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 100 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 100 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 200 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 200 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 200 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 200 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 200 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 200 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 200 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 200 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-syn ../data/ppdb-syn.2.txt 200 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 50 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 50 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 50 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 50 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 50 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 50 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 50 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 50 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 50 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 75 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 75 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 75 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 75 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 75 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 75 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 75 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 75 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 75 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 100 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 100 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 100 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 100 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 100 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 100 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 100 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 100 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 100 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 200 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 200 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 200 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 200 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 200 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 200 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 200 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 200 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-ant ../data/ppdb-ant.2.txt 200 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 50 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 50 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 50 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 50 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 50 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 50 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 50 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 50 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 50 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 75 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 75 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 75 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 75 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 75 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 75 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 75 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 75 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 75 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 100 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 100 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 100 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 100 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 100 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 100 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 100 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 100 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 100 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 200 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 200 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 200 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 200 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 200 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 200 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 200 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 200 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-pro ../data/ppdb-pro.2.txt 200 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 50 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 50 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 50 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 50 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 50 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 50 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 50 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 50 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 50 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 75 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 75 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 75 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 75 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 75 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 75 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 75 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 75 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 75 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 100 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 100 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 100 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 100 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 100 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 100 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 100 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 100 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 100 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 200 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 200 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 200 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 200 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 200 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 200 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 200 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 200 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-syn-ant-pro ../data/ppdb-syn-ant-pro.2.txt 200 25 MAX
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 50 25 RAND
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 50 25 MIX
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 50 25 MAX
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 50 25 RAND
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 50 25 MIX
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 50 25 MAX
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 50 25 RAND
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 50 25 MIX
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 50 25 MAX
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 75 25 RAND
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 75 25 MIX
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 75 25 MAX
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 75 25 RAND
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 75 25 MIX
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 75 25 MAX
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 75 25 RAND
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 75 25 MIX
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 75 25 MAX
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 100 25 RAND
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 100 25 MIX
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 100 25 MAX
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 100 25 RAND
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 100 25 MIX
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 100 25 MAX
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 100 25 RAND
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 100 25 MIX
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 100 25 MAX
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 200 25 RAND
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 200 25 MIX
sh train.sh 0.0001 1.0 words-wn-syn ../data/wn-syn-only.2.txt 200 25 MAX
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 200 25 RAND
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 200 25 MIX
sh train.sh 1e-05 1.0 words-wn-syn ../data/wn-syn-only.2.txt 200 25 MAX
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 200 25 RAND
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 200 25 MIX
sh train.sh 1e-06 1.0 words-wn-syn ../data/wn-syn-only.2.txt 200 25 MAX
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 50 25 RAND
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 50 25 MIX
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 50 25 MAX
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 50 25 RAND
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 50 25 MIX
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 50 25 MAX
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 50 25 RAND
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 50 25 MIX
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 50 25 MAX
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 75 25 RAND
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 75 25 MIX
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 75 25 MAX
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 75 25 RAND
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 75 25 MIX
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 75 25 MAX
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 75 25 RAND
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 75 25 MIX
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 75 25 MAX
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 100 25 RAND
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 100 25 MIX
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 100 25 MAX
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 100 25 RAND
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 100 25 MIX
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 100 25 MAX
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 100 25 RAND
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 100 25 MIX
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 100 25 MAX
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 200 25 RAND
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 200 25 MIX
sh train.sh 0.0001 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 200 25 MAX
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 200 25 RAND
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 200 25 MIX
sh train.sh 1e-05 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 200 25 MAX
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 200 25 RAND
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 200 25 MIX
sh train.sh 1e-06 1.0 words-wn-syn-ant ../data/wn-syn-ant-only.2.txt 200 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 50 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 50 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 50 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 50 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 50 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 50 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 50 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 50 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 50 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 75 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 75 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 75 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 75 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 75 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 75 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 75 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 75 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 75 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 100 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 100 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 100 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 100 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 100 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 100 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 100 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 100 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 100 25 MAX
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 200 25 RAND
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 200 25 MIX
sh train.sh 0.0001 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 200 25 MAX
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 200 25 RAND
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 200 25 MIX
sh train.sh 1e-05 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 200 25 MAX
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 200 25 RAND
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 200 25 MIX
sh train.sh 1e-06 1.0 words-ppdb-xxl ../data/ppdb-new.xxl.2.txt 200 25 MAX
