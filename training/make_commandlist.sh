#python make_tti_commands_single.py ../data/ppdb-new.2.txt words-ppdb 0.05 "../models/skipwiki25_words_" > word_commands_test.txt
#python make_tti_commands_single.py ../data/ppdb-syn.2.txt words-ppdb-syn 0.05 "../models/skipwiki25_words_" >> word_commands_test.txt
#python make_tti_commands_single.py ../data/ppdb-ant.2.txt words-ppdb-ant 0.05 "../models/skipwiki25_words_" >> word_commands_test.txt
#python make_tti_commands_single.py ../data/ppdb-pro.2.txt words-ppdb-pro 0.05 "../models/skipwiki25_words_" >> word_commands_test.txt
#python make_tti_commands_single.py ../data/ppdb-syn-ant-pro.2.txt words-ppdb-syn-ant-pro 0.05 "../models/skipwiki25_words_" >> word_commands_test.txt

python make_tti_commands_single.py ../data/ppdb-new.2.txt words-ppdb 1.0 "../models/skipwiki25_words_" > word_commands.txt
python make_tti_commands_single.py ../data/ppdb-syn.2.txt words-ppdb-syn 1.0 "../models/skipwiki25_words_" >> word_commands.txt
python make_tti_commands_single.py ../data/ppdb-ant.2.txt words-ppdb-ant 1.0 "../models/skipwiki25_words_" >> word_commands.txt
python make_tti_commands_single.py ../data/ppdb-pro.2.txt words-ppdb-pro 1.0 "../models/skipwiki25_words_" >> word_commands.txt
python make_tti_commands_single.py ../data/ppdb-syn-ant-pro.2.txt words-ppdb-syn-ant-pro 1.0 "../models/skipwiki25_words_" >> word_commands.txt

python make_tti_commands_single.py ../data/wn-syn-only.2.txt words-wn-syn 1.0 "../models/skipwiki25_words_" >> word_commands.txt
python make_tti_commands_single.py ../data/wn-syn-ant-only.2.txt words-wn-syn-ant 1.0 "../models/skipwiki25_words_" >> word_commands.txt
python make_tti_commands_single.py ../data/ppdb-new.xxl.2.txt words-ppdb-xxl 1.0 "../models/skipwiki25_words_" >> word_commands.txt

#python make_tti_commands_single_gouda.py ../data/ppdb-new.2.txt words-ppdb 0.05 "../models/skipwiki25_words_" > word_commands_test_gouda.txt
#python make_tti_commands_single_gouda.py ../data/ppdb-syn.2.txt words-ppdb-syn 0.05 "../models/skipwiki25_words_" >> word_commands_test_gouda.txt
#python make_tti_commands_single_gouda.py ../data/ppdb-ant.2.txt words-ppdb-ant 0.05 "../models/skipwiki25_words_" >> word_commands_test_gouda.txt
#python make_tti_commands_single_gouda.py ../data/ppdb-pro.2.txt words-ppdb-pro 0.05 "../models/skipwiki25_words_" >> word_commands_test_gouda.txt
#python make_tti_commands_single_gouda.py ../data/ppdb-syn-ant-pro.2.txt words-ppdb-syn-ant-pro 0.05 "../models/skipwiki25_words_" >> word_commands_test_gouda.txt

python make_tti_commands_single_gouda.py ../data/ppdb-new.2.txt words-ppdb 1.0 "../models/skipwiki25_words_" > word_commands_gouda.txt
python make_tti_commands_single_gouda.py ../data/ppdb-syn.2.txt words-ppdb-syn 1.0 "../models/skipwiki25_words_" >> word_commands_gouda.txt
python make_tti_commands_single_gouda.py ../data/ppdb-ant.2.txt words-ppdb-ant 1.0 "../models/skipwiki25_words_" >> word_commands_gouda.txt
python make_tti_commands_single_gouda.py ../data/ppdb-pro.2.txt words-ppdb-pro 1.0 "../models/skipwiki25_words_" >> word_commands_gouda.txt
python make_tti_commands_single_gouda.py ../data/ppdb-syn-ant-pro.2.txt words-ppdb-syn-ant-pro 1.0 "../models/skipwiki25_words_" >> word_commands_gouda.txt

python make_tti_commands_single_gouda.py ../data/wn-syn-only.2.txt words-wn-syn 1.0 "../models/skipwiki25_words_" >> word_commands_gouda.txt
python make_tti_commands_single_gouda.py ../data/wn-syn-ant-only.2.txt words-wn-syn-ant 1.0 "../models/skipwiki25_words_" >> word_commands_gouda.txt
python make_tti_commands_single_gouda.py ../data/ppdb-new.xxl.2.txt words-ppdb-xxl 1.0 "../models/skipwiki25_words_" >> word_commands_gouda.txt
