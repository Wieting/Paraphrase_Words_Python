import sys
import os

data = sys.argv[1]
fname = sys.argv[2]
frac = sys.argv[3]
outfile = sys.argv[4]

lambda2=[0.0001,0.00001,0.000001]
bs=[50,75,100,200]
type = ["RAND","MIX","MAX"]

for j in range(len(bs)):
    for i in range(len(lambda2)):
        for k in range(len(type)):
            cmd = str(lambda2[i])+" "+frac+" "+fname+" "+data+" "+str(bs[j])+" 25 "+type[k]
            cmd = "sh train_gouda.sh "+cmd
            print cmd
