import sys
import operator
import os.path

f = sys.argv[1]
best = 0
total = 0

d={}
best_dev=0
pred=""
for i in range(1000):
    j = i + 1
    ff = f + "." + str(j)
    if(not os.path.isfile(ff)):
        continue
    ff = open(ff,'r')
    lines = ff.readlines()
    ct =0
    for ll in lines:
        if('skipwiki25' in ll):
            arr = ll.split()
            name = ll
            score = float(arr[4])
            d[name]=score
            test_score = 2*float(arr[2]) - float(arr[3])
            if(test_score > best_dev):
                pred = ll
                best_dev = test_score
            total += 1
            ct += 1
sorted_d = sorted(d.items(), key=operator.itemgetter(1))
for i in sorted_d:
    print i[0], i[1]
print best
print total
print pred